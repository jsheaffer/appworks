/**
 * Update student info in Alma
 * @author JS
 *
 * _@NApiVersion 2.1
 * _@NScriptType WorkflowActionScript
 */


define([
    'N/record', 
    'N/search', 
    'N/https', 
    './lss_library'
],
function( 
    record, 
    search, 
    https, 
    library 
) {
return {
    onAction: function( context ) {
        
        var applicant = context.newRecord;
        var Entity  = applicant.getValue({ fieldId: 'entitytitle' });
        
        log.audit({
            title: Entity,
            details: 'New Student'
        });
        //  Get Environment variables
        var environment = library.getEnvironment();
        //  Build Post Data
        var data = {
            id            : applicant.id,
            firstName     : applicant.getValue({ fieldId: 'firstname' }),
            lastName      : applicant.getValue({ fieldId: 'lastname' }),
            email         : applicant.getValue({ fieldId: 'custentitystudentemail' }),
            gender        : applicant.getValue({ fieldId: 'custentitygender' }),
            birthDate     : applicant.getValue({ fieldId: 'custentitystudentbirthdate' }),
            grade         : applicant.getValue({ fieldId: 'custentitygrade' }),
            international : applicant.getValue({ fieldId: 'custentity_international_student' }),
            secretCode    : environment.secret
        };
        
        log.debug({
            title: Entity + ' | Data',
            details: data
        });
        
        //  Send Applicant
        try {
        
            var response = https.post({
                url 	: environment.url,
                headers : environment.headers,
                body    : JSON.stringify( data )
            });
            
            var body = JSON.parse( response.body );
            
            if ( body.success ) {
                
                applicant.setValue({
                    fieldId: 'custentity_in_appworks',
                    value: true
                });
        
                log.audit({
                    title: Entity + ' | Applicant Added',
                    details: response
                });
            }
            else {
        
                log.error({
                    title: Entity + ' | Applicant Not Added',
                    details: response
                });
            }
        }
        catch( error ) {
        
            log.error({
                title: Entity + ' | Could not add applicant',
                details: error
            });
        }
    }
    
};    
});