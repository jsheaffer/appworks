/**
 * Laurel Springs Applicant Library functions
 * @author JS
 *
 * _@NApiVersion 2.1
 */

define([
    'N/runtime'
],
function(
    runtime
) {
return {
    
    getEnvironment  : function() {
        //  Determine environment
        switch ( runtime.envType ) {

            case 'PRODUCTION':
                return {
                    url     : 'https://www.experiencelaurelsprings.com/api/user/create',
                    secret  : 'dsaU35zjoDFG01nnjfnhPOLSD1898yw',
                    headers : {
                        'Content-Type' : 'application/json'
                    }
                };
                break;

            case 'SANDBOX':
                return {
                    url     : 'https://ls-onboarding-qa.herokuapp.com/api/user/create',
                    secret  : 'asdU13zjlASD01czxqOI910SD08ysjaw',
                    headers : {
                        'Content-Type' : 'application/json'
                    }
                };
                break;
        }
    }
    
};
});